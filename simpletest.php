<?php
require_once('turnstile.php');
$turnstile = new turnstileClass();




//EPIC
//Background
//It is necessary to build the domain code so that we can approximate the know how level.
//Delivery is proven by unit tests of the associated user stories and additional extensions/validations.
//
//MAIN SUCCESS SCENARIO
//-----------------------------------------
//A customer inserts a coin and the turnstile unlocks
//Customer passes and turnstile locks
//
//

$turnstile->reset();
$turnstile->log('1.');
$turnstile->insertCoin();
$turnstile->passThru();


//Extensions
//-----------------------------------------
//1.a Customer passes without inserting a coin
//The turnstile will raise an alarm
//
//2.b Customer inserts a coin again
//Turnstile remains unlocked
//

$turnstile->reset();
$turnstile->log('2.');
$turnstile->passThru();
$turnstile->insertCoin();


//-----------------------------------------
//User Story: Unlocking the turnstile
//SCENARIO
//
//Given the turnstile is locked
////When I add a coin
//
//The turnstile will unlock

$turnstile->reset();
$turnstile->log('3.');
$turnstile->insertCoin();


//-----------------------------------------
//User Story: Locking the turnstile
//SCENARIO
//
//Given the turnstile is unlocked
//
//When I pass trough the turnstile
//
//The turnstile will lock
//

$turnstile->reset();
$turnstile->log('4.');
$turnstile->turnstileState(turnstileClass::STATE_unlocked);
$turnstile->passThru();



//-----------------------------------------
//User Story: Raising an alarm
//SCENARIO
//
//Given the turnstile is locked
//When I pass
//An alarm is triggered
//And If I add a coin
//The alarm will end
//
$turnstile->reset();
$turnstile->log('5.');
$turnstile->passThru();
$turnstile->insertCoin();


//-----------------------------------------
//User Story: Gracefuly eating money
//SCENARIO
//
//Given the turnstile is locked
//When I add a coin
//And then another coin
//The turnstile will be unlocked
//And If I pass The turnstile will be locked

$turnstile->reset();
$turnstile->log('6.');
$turnstile->insertCoin();
$turnstile->insertCoin();
$turnstile->passThru();


$turnstile->log('END');
