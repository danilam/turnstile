<?php
require_once('storage.php');
require_once('logger.php');

class turnstileClass
{


    const STATE_locked = 'locked';
    const STATE_unlocked = 'unlocked';

    /**
     * @var webStorage|null
     */
    protected $storage = null;

    /**
     * @var loggerClass|null
     */
    protected $logger = null;


    /**
     * Silent mode
     * @var bool
     */
    protected $silent = false;

    /**
     * Logger
     * @param $message
     */
    function log($message) {
        if (! $this->silent) {
            $this->logger->log($message);
        }
    }
    
    function reset()
    {
        $this->log('');
        $this->log('Reset states');
        $this->log('-------------------------');
        $this->alarmState(false);
        $this->turnstileState(static::STATE_locked);
    }




    function __construct($silent = false)
    {
        $this->storage = new webStorage();
        $this->logger = new loggerClass();
        $this->silent = $silent;

        //Turnstile state
        //default value is "locked"
        if (is_null($this->turnstileState())) {
            $this->log('set default value to Turnstile state');
            $this->turnstileState(static::STATE_locked);
        }

        //Alarm state
        //default value is false
        if (is_null($this->alarmState())) {
            $this->log('set default value to Alarm state');
            $this->alarmState(false);
        }

        $this->log('---------------------------------');
        $this->log('App starts. Current states:');
        //Log the current states
        $this->log('turnstile is ' . $this->turnstileState());
        $this->log('alarm is ' . ($this->alarmState() ? 'ON' : 'OFF'));
        $this->log('---------------------------------');

    }

    function __destruct()
    {
        $this->log('Ending app');
        $this->log('---------------------------------');
    }


    /**
     * Set or get turnstile state
     * @param null $state
     * @return mixed|null
     */
    function turnstileState($state = null)
    {
        if (is_null($state)) {
            return $this->storage->state;
        }
        if ($this->storage->state != $state) {
            $this->storage->state = $state;
            $this->log('turnstile is ' . $state);
        }
        return $state;
    }

    /**
     * Set or get alarm state
     * @param null $is_alarm
     * @return mixed|null
     */
    function alarmState($is_alarm = null)
    {
        if (is_null($is_alarm)) {
            return $this->storage->alarm;
        }
        if ($this->storage->alarm != $is_alarm) {
            $this->storage->alarm = $is_alarm;
            $this->log('alarm is ' . ($is_alarm ? 'ON' : 'OFF'));
        }
        return $is_alarm;
    }

    /**
     * Insert the coin
     */
    function insertCoin()
    {
        $this->log('coin was inserted');
        $this->alarmState(false);
        $this->turnstileState(static::STATE_unlocked);
    }


    /**
     * passing through the turnstile
     */
    function passThru()
    {
        $this->log('passing through the turnstile');
        if ($this->turnstileState() == static::STATE_unlocked) {
            $this->turnstileState(static::STATE_locked);
            return;
        }
        //turnstile was closed
        $this->alarmState(true);
    }

}