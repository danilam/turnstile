<?php
require_once('turnstile.php');

use PHPUnit\Framework\TestCase;


class turnstileTests extends TestCase
{
    /**
     * @var turnstileClass
     */
    private $turnstile;

    protected function setUp()
    {
        $this->turnstile = new turnstileClass(true);
    }

    protected function tearDown()
    {
        $this->turnstile = NULL;
    }

    public function testN1()
    {

        //A customer inserts a coin and the turnstile unlocks
        //Customer passes and turnstile locks

        $this->turnstile->reset();
        $this->assertEquals($this->turnstile->turnstileState(), turnstileClass::STATE_locked);
        $this->turnstile->insertCoin();
        $this->assertEquals($this->turnstile->turnstileState(), turnstileClass::STATE_unlocked);
        $this->turnstile->passThru();
        $this->assertEquals($this->turnstile->turnstileState(), turnstileClass::STATE_locked);
    }


    public function testN2()
    {

        //1.a Customer passes without inserting a coin
        //The turnstile will raise an alarm
        //
        //2.b Customer inserts a coin again
        //Turnstile remains unlocked
        //

        $this->turnstile->reset();
        $this->assertEquals($this->turnstile->alarmState(), false);
        $this->assertEquals($this->turnstile->turnstileState(), turnstileClass::STATE_locked);

        $this->turnstile->passThru();
        $this->assertEquals($this->turnstile->alarmState(), true);

        $this->turnstile->insertCoin();
        $this->assertEquals($this->turnstile->turnstileState(), turnstileClass::STATE_unlocked);
    }


    public function testN3()
    {
        //User Story: Unlocking the turnstile
        //SCENARIO
        //
        //Given the turnstile is locked
        ////When I add a coin
        //
        //The turnstile will unlock

        $this->turnstile->reset();
        $this->assertEquals($this->turnstile->turnstileState(), turnstileClass::STATE_locked);

        $this->turnstile->insertCoin();
        $this->assertEquals($this->turnstile->turnstileState(), turnstileClass::STATE_unlocked);
    }


    public function testN4()
    {
        //User Story: Locking the turnstile
        //SCENARIO
        //
        //Given the turnstile is unlocked
        //
        //When I pass trough the turnstile
        //
        //The turnstile will lock
        //

        $this->turnstile->reset();
        $this->turnstile->turnstileState(turnstileClass::STATE_unlocked);
        $this->assertEquals($this->turnstile->turnstileState(), turnstileClass::STATE_unlocked);

        $this->turnstile->passThru();
        $this->assertEquals($this->turnstile->turnstileState(), turnstileClass::STATE_locked);


    }


    public function testN5() {
        //User Story: Raising an alarm
        //SCENARIO
        //
        //Given the turnstile is locked
        //When I pass
        //An alarm is triggered
        //And If I add a coin
        //The alarm will end
        //
        $this->turnstile->reset();
        $this->assertEquals($this->turnstile->turnstileState(), turnstileClass::STATE_locked);

        $this->turnstile->passThru();
        $this->assertEquals($this->turnstile->alarmState(), true);

        $this->turnstile->insertCoin();
        $this->assertEquals($this->turnstile->alarmState(), false);
    }


    public function testN6() {

        //User Story: Gracefuly eating money
        //SCENARIO
        //
        //Given the turnstile is locked
        //When I add a coin
        //And then another coin
        //The turnstile will be unlocked
        //And If I pass The turnstile will be locked

        $this->turnstile->reset();
        $this->assertEquals($this->turnstile->turnstileState(), turnstileClass::STATE_locked);

        $this->turnstile->insertCoin();
        $this->assertEquals($this->turnstile->turnstileState(), turnstileClass::STATE_unlocked);
        $this->turnstile->insertCoin();
        $this->assertEquals($this->turnstile->turnstileState(), turnstileClass::STATE_unlocked);
        $this->turnstile->passThru();
        $this->assertEquals($this->turnstile->turnstileState(), turnstileClass::STATE_locked);
    }


}