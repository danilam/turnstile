<?php


class webStorage
{
    const VARS_FILENAME = __DIR__ . '/vars.json';
    protected $vars = [];

    function __set($name, $value)
    {
        $this->vars[$name] = $value;
        $this->save();
    }

    function __get($name)
    {
        return (isset($this->vars[$name])) ? $this->vars[$name] : null;
    }


    function save()
    {
        file_put_contents(static::VARS_FILENAME, json_encode($this->vars));
    }

    function load()
    {
        if (file_exists(static::VARS_FILENAME)) {
            $tmp = json_decode(file_get_contents(static::VARS_FILENAME), true);
            if (is_array($tmp)) {
                $this->vars = $tmp;
            } else {
                //todo raise exception
            }
        }
    }

    function __construct()
    {
        $this->load();
    }

    function __destruct()
    {
        $this->save();
    }

}